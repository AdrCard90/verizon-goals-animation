const BubbleOverlay = ({ handleClick }) => {
    // we need to make sure this overlay is above the canvas
    // check why is restarting the bubbles after opening the overlay
    return (
        <div style={{
            position: "absolute",
            top: 0,
            left: 0,
            width: "100vw",
            height: "100vh",
            opacity: 0.5,
            backgroundColor: "black",
        }}
            onClick={handleClick}
        />
    )
};

export default BubbleOverlay;