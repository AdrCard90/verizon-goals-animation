import { useState } from "react";
import { useNavigate} from "react-router-dom";
import './fonts.css';

const Iphone = () =>{
    const [val, setVal] = useState(400)
    const navigate = useNavigate();
    return (
        <>
            <img src='./EditGoal.svg' style={{width:'100vw', height:'auto'}}/>
            {/* Back Button */}
            <a style={{position:'absolute', opacity:0, width:32, height:32, top:20, left:24}} onClick={()=>navigate('/')}/>

            <div style={{position:'absolute', top: '408px', margin: 'auto 24px', width: 'calc(100% - 48px)',height: '64px', display:'flex', flexDirection:'column', borderBottom:'solid 1px black'}}>
                <label style={{color:'#6F7171', fontWeight:'bold', fontSize:'11px'}}>Meta de ahorro</label>
                <input 
                    type='number'
                    style={{flex:1, marginTop:'6px', border: 'solid 1px #D8DADA', padding:'0 14px', fontSize:'16px', fontWeight:'400', color:'#000'}}
                    placeholder="Cuál es tu meta de ahorro?"
                    value={val}
                    onChange={(e)=>setVal(e.target.val)}
                />
            </div>

            <a style={{position:'absolute', opacity:0, width:110, height:50, bottom:22,left:'35%'}} onClick={()=>navigate('/')}/>

        </>
    )
}

export default Iphone