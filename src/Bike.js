import { useNavigate } from "react-router-dom";
import './fonts.css';

const Bike = () =>{
    const navigate = useNavigate();
    return (
        <>
            <img src='./Bike.svg' style={{width:'100vw', height:'auto'}}/>
            {/* Back Button */}
            <a style={{position:'absolute', opacity:1, width:64, height:64, top:20, left:24}} onClick={()=>navigate('/')}/>
        </>
    )
}

export default Bike