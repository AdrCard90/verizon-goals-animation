import { useNavigate } from "react-router-dom";
import './fonts.css';

const Iphone = () =>{
    const navigate = useNavigate();
    return (
        <>
            <img src='./Iphone.svg' style={{width:'100vw', height:'auto'}}/>
            {/* Back Button */}
            <a style={{position:'absolute', opacity:1, width:64, height:64, top:20, left:24}} onClick={()=>navigate('/')}/>
            {/* Edit Button */}
            <a style={{position:'absolute', opacity:0, width:64, height:64, top:70, right:20}} onClick={()=>navigate('/edit')}/>
        </>
    )
}

export default Iphone