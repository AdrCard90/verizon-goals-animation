import { useEffect, useState } from 'react';
import Matter, {
    Engine,
    Render,
    Runner,
    Body,
    Bodies,
    Composite,
    Mouse,
    MouseConstraint,
    Events,
    Vector,
    Bounds
} from 'matter-js';
import MatterAttractors from 'matter-attractors';
import BubbleOverlay from './components/BubbleOverlay';
import RadialMenu from './components/RadialMenu';

import './fonts.css';
import "./App.css";

import { hexToHSL } from "./helpers";

import { useNavigate } from "react-router-dom";


Matter.use(MatterAttractors);


const userGoals = [
    {
        title:'Películas',
        icon: './count-down_blk.svg',
        backgroundType: 'color',
        background: '#D90368',
        progress:80,
        total:50,
        overlay:true,
        overdue:true,
    },
    {
        title:'Mario Kart',
        // icon: './checkmark.svg',
        backgroundType: 'image',
        background: './Mario.png',
        progress:80,
        total:100,
    },
    {
        title:'Groot',
        icon: './count-down_blk.svg',
        backgroundType: 'image',
        background: './Groot.png',
        progress:30,
        total:100,
        overdue:true
    },
    {
        title:'Switch',
        icon: './checkmark.svg',
        backgroundType: 'image',
        background: './Switch.png',
        progress:100,
        total:120,
    },
    {
        title:'Bicicleta',
        icon: './Bike-icon.svg',
        backgroundType: 'color',
        background: '#F2ABCD',
        progress:0,
        total:98,
    },
    {
        title:'Nuevo Celular',
        icon: './Phone.svg', //Broken icon
        backgroundType: 'color',
        background: '#ABD8EF',
        progress:27.4,
        total:400
    }
    // {
    //     title:'ComiCon',
    //     icon: './virtual-reality-blk.svg',
    //     backgroundType: 'color',
    //     background: '#ABD8EF',
    //     progress:75,
    //     total:155,
    // }
]
// const placeholderGoals = [{title:'Food fest', icon:'./agriculture-vineyard.svg'}, {title:'learning', icon:'education.svg'},{title:'bicicle', icon:'sports-bike.svg'},{title:'Skis', icon:'sports-skis.svg'}, {title:'HBD', icon:'gift.svg'}]

const placeholderGoals = [
    {
        title:'Tenis',
        icon:'./Tennis.svg'
    },
    {
        title:'Festival de Comida',
        icon:'./Grapes.svg'
    },
    {
        title:'ComicCon',
        icon:'./Calendar.svg',
        progress:70
    },
    {
        title:'Cumpleaños',
        icon:'./gift.svg',
        progress:60
    }
]
const canvasSize = {
    width: document.body.clientWidth,
    height: document.body.clientHeight-65
};

const App = () => {
    // Este estado se usa para mostrar el overlay en el evento de long press, revisar si hay una mejor alternativa
    const [showMenu, setShowMenu] = useState(false);

    const navigate = useNavigate()

    const engine = Engine.create({
        gravity: {
            scale: 0,
        },
    });

    useEffect(() => {
        const render = Render.create({
            element: document.getElementById("goals-animation"),
            engine,
            options: {
                width: canvasSize.width,
                height: canvasSize.height,
                wireframes: false,
                hasBounds: true,
                background:'#F6F6F6'
            },
        });

        const runner = Runner.create();

        let ctx;
        const init = async () => {
            // Funcion que crea una burbuja
            const createCanvasGoal = async (type='color',background, radius, icon, title, text, tapped=false, overlay=false) => {
                //Esta funcion agrega la imagen para ser usada como background de la burbuja
                const generateImage = (imageUrl) => {
                    return new Promise((resolve, reject) => {
                        const image = new Image();
                        image.crossOrigin = "Anonymous";
                        image.onload = () => resolve(image);
                        image.src = imageUrl;
                    });
                };


                const getStrokeColor = (background) => {
                    if(!background) return '#FFF'
                    const {h,s,l}  = hexToHSL(background,true)
                    if(s>75 || l<50)return '#FFF'
                    const darken =  `hsl(${h},${90}%,${25}%)`
                    // console.log(background,darken)
                    return darken
                }

                //word wrap for texts
                const printAtWordWrap = ( text, x, y, lineHeight, fitWidth)=>
                {
                    fitWidth = fitWidth || 0;
                    
                    if (fitWidth <= 0)
                    {
                        ctx.fillText( text, x, y );
                        return;
                    }
                    var words = text.split(' ');
                    var currentLine = 0;
                    var idx = 1;
                    while (words.length > 0 && idx <= words.length)
                    {
                        var str = words.slice(0,idx).join(' ');
                        var w = ctx.measureText(str).width;
                        if ( w > fitWidth )
                        {
                            if (idx==1)
                            {
                                idx=2;
                            }
                            ctx.fillText( words.slice(0,idx-1).join(' ') + '...', x, y + (lineHeight*currentLine) );
                            currentLine++;
                            words = words.splice(idx-1);
                            idx = 1;
                        }
                        else
                        {idx++;}
                    }
                    if  (idx > 0 && currentLine === 0){
                        ctx.fillText( words.join(' '), x, y + (lineHeight*currentLine) );
                    }
                }
                const strokeColor = type==='placeholder'?"#6F7171":getStrokeColor(type==='color'?background:null)

                const drawing = document.createElement("canvas");
                drawing.width = radius * 2;
                drawing.height = radius * 2;
                ctx = drawing.getContext("2d");
                ctx.beginPath();
                ctx.arc(radius, radius, radius, 0, Math.PI * 2);
                ctx.closePath();
                
                if(type==='color'){
                    //Estas lineas de codigo se usan para agregar un color a la burbuja en caso de que no se use background
                    ctx.fillStyle = background;
                    ctx.fill();

                    //dark overlay
                    if(overlay){
                        ctx.globalAlpha = 0.5;
                        ctx.fillStyle = '#000';
                        ctx.arc(radius, radius, radius, 0, Math.PI * 2);
                        ctx.closePath();
                        ctx.fill();
                        ctx.globalAlpha = 1;
                    }

                }
                else if(type === 'image'){
                    //Este codigo agrega la imagen generada al background de la burbuja
                    ctx.clip();
                    ctx.stroke();
                    const img = await generateImage(background);
                    ctx.drawImage(img, 0, 0,radius*2,radius*2);
                    
                    //dark overlay
                    ctx.globalAlpha = 0.25;
                    ctx.fillStyle = '#000';
                    ctx.fillRect(0, 0, radius*2, radius*2);
                    ctx.globalAlpha = 1;
                }
                else if (type === 'placeholder'){
                    ctx.beginPath();
                    ctx.setLineDash([5, 5]);
                    ctx.strokeStyle = "#D8DADA";
                    ctx.lineWidth = 2;
                    ctx.beginPath();
                    ctx.arc(radius, radius, radius, 0, Math.PI * 2);
                    ctx.closePath();
                    ctx.stroke();
                }
                

                if(icon && !tapped){
                    const iconimg = await generateImage(icon);
                    //FIXME add icon color
                    ctx.drawImage(iconimg, radius*0.75, radius*0.85, radius*0.5, radius*0.5);
                }else if(tapped){
                    const iconimg = await generateImage('./button_go.svg');
                    ctx.drawImage(iconimg, radius*0.4, radius*0.85, radius*1.2, radius*0.5);
                }

                //Estas lineas posicionan el texto y el icono para una burbuja, hay que realizar los cambios para adaptar esto al tamaño y posicion correctos
                ctx.fillStyle = "#fff";
                ctx.textAlign = "center";

                ctx.fillStyle = strokeColor
                if(title){
                    ctx.font = "bold 10pt sans-serif";
                    printAtWordWrap(title, radius, radius * (icon||tapped?0.65:1.1), 15, radius*2*0.75)
                    // ctx.fillText(title, radius, radius * (icon?0.5:1.1));
                }
                if(text){
                    ctx.font = "bold 12pt sans-serif";
                    printAtWordWrap(text, radius, radius * (tapped?1.75:1.5), 15, 90);
                }
                return drawing.toDataURL("image/png");
            };

            const transparentConfig = {
                isStatic: true,
                render: {
                    fillStyle: 'transparent',
                    strokeStyle: 'transparent',
                    lineWidth: 2
                }
            };

            // Esto crea un elemento que sirve como pared superior para que las burbujas no suban mas del limite correcto, conversar con diseño sobre si
            // es necesario un limite inferior tambien
            const limits = [
                // Top side limit
                Bodies.rectangle(0, 0, canvasSize.width * 20, 240, {...transparentConfig, label:'_bound-top'}),
                Bodies.rectangle(-20, 0,  50, canvasSize.height * 2, {...transparentConfig, label:'_bound-left'}),
                Bodies.rectangle(0, canvasSize.height+20,  canvasSize.width*50, 200, {...transparentConfig, label:'_bound-bottom'}),
            ];

            const bubbleSizes=(progress)=>{
                const map = (value, x1, y1, x2, y2) => (value - x1) * (y2 - x2) / (y1 - x1) + x2;
                return map(progress,0,100,60,86)
            }

            // const bubbleSizes = {
            //     0: 28,
            //     25: 36,
            //     50: 44,
            //     75: 52,
            //     100: 60
            // };
            // const bubbleProgress = Object.keys(bubbleSizes);

            // const fixedGoal = Bodies.circle(canvasSize.width / 2, 75, bubbleSizes[75], {
            //     isStatic: true,
            //     render: {
            //         fillStyle: 'silver',
            //     },
            // });

            // Burbuja detras de la burbuja fija que atrae a las demas
            const attractorBody = Body.create({
                    parts: [Bodies.circle(90, 180, bubbleSizes(75), {
                        render: {
                            fillStyle: '#000000',
                            sprite: {
                                texture: await createCanvasGoal(
                                    'color',
                                    "#000",
                                    bubbleSizes(75),
                                    null,
                                    'Disponible',
                                    '$100'
                                )
                            },
                            lineWidth: 1,
                        },
                        
                    }), Bodies.circle(90, 180, bubbleSizes(75) + 3, {
                        ...transparentConfig,
                        render:{
                            ...transparentConfig.render,
                            strokeStyle:'#57C880'
                        }
                    })],
                    isStatic: true,
                    label:'attractor',
                    plugin: {
                        attractors: [
                            function (bodyA, bodyB) {
                                return{
                                    x: (bodyA.position.x - bodyB.position.x) * 1e-5,
                                    y: (bodyA.position.y - bodyB.position.y) * 1e-5,
                                };
                            },
                        ],
                    },
                    //parts: [goal, goalSeparator, rectangle],
                    label: '_attractor',
            });
            
            

            const goalBubbles = []//[fixedGoal];



            const createGoal = async (uGoal,tap=false,position=null,text='', priority=1)=>{
                // console.log(position)
                const randomPositionX = (Math.floor(Math.random() * (canvasSize.width-(canvasSize.width*1.5)+ 1) + canvasSize.width))+1000;
                const randomPositionY = Math.floor(canvasSize.height/2 +100);
                const randomSize = uGoal.progress != null ? uGoal.progress : Math.round(Math.random()*100);
                const randomBubbleSize = bubbleSizes(randomSize);
                const texture = await createCanvasGoal(
                    uGoal.backgroundType || 'placeholder',
                    uGoal.background,
                    randomBubbleSize,
                    uGoal.icon,
                    uGoal.title,
                    text,
                    tap,
                    !!uGoal.overlay               
                    );
                const goal = Bodies.circle(position?.x||randomPositionX, position?.y||randomPositionY, randomBubbleSize, {
                    render: {
                        sprite: {
                            texture: texture
                        }
                    }
                });
                const goalSeparator = Bodies.circle(position?.x||randomPositionX, position?.y||randomPositionY, randomBubbleSize + 6, {
                    ...transparentConfig,
                    isStatic: false,
                });

                const circle = Body.create({
                    parts: [goal, goalSeparator],
                    //parts: [goal, goalSeparator, rectangle],
                    label: uGoal.backgroundType?uGoal.title:Math.floor(Math.random() * 100), //TODO: replace this to unique id
                });
                return circle
            }


            // Aca se generan 15 burbujas, esto debe cambiarse para integrar la data con la cantidad de burbujas correcta
            for await (const uGoal of [...userGoals, ...placeholderGoals]) {
                const circle = await createGoal(uGoal,null,null,null,3)
                goalBubbles.push(circle);
            };

            Composite.add(engine.world, [attractorBody, ...limits, ...goalBubbles]);

            var canvasMouse = Mouse.create(document.querySelector('canvas'));

            var mConstraint = MouseConstraint.create(engine, { mouse: canvasMouse });

            render.mouse = canvasMouse

            let longPressTimer = null, shortPressTimer = null, isShortPress=false;

            // funcion para probar la idea de erik de dividir el canvas en 4 areas para detectar en que parte se hizo click
            const getAreaClicked = ({ x, y }) => {
                let area;
                const xAxis = canvasSize.height / 2;
                const yAxis = canvasSize.width / 2;
                const topArea = y <= xAxis;
                const bottomArea = y > xAxis;
                const leftArea = x <= yAxis;
                const rightArea = x > yAxis;

                if (topArea && rightArea) {
                    area = "firstCuadrant";
                } else if (bottomArea && rightArea) {
                    area = "secondCuadrant";
                } else if (bottomArea && leftArea) {
                    area = "thirdCuadrant";
                } else {
                    area = "fourthCuadrant";
                }

                return area;
            };

            const callLongPress = (bubble) => {
                // console.log('LONG PRESS TRIGGERED', bubble)
                // setShowMenu(true);
            };

            let activeBubble = null
            const callShortPress = async (bubble) =>{
                if(typeof bubble.label==='string' && activeBubble?.label === bubble.label){
                    // console.log('You clicked ' + bubble.label + '!!!!!!!!!!!!!!!!!!')
                    if(bubble.label === 'Bicicleta')navigate('/bike')
                    else if(bubble.label === 'Nuevo Celular')navigate('/Iphone')
                    return //prevent any logic
                }
                await clearActiveBubbles()
                if(typeof bubble.label==='string'){
                    const template = userGoals.find(u=>u.title === bubble.label)
                    if(template){
                        const newBubble = await createGoal(template,true,bubble.position,template.overdue?'Vencido':template.progress>=100?'Completado':`$${Math.ceil((template.progress/100)*template.total)}/$${template.total}`)
                        Composite.remove(engine.world,[bubble])
                        Composite.add(engine.world,[newBubble])
                        activeBubble=newBubble
                    }
                }
            };
            const clearActiveBubbles= async() => {
                if(activeBubble){
                    const template = userGoals.find(u=>u.title === activeBubble.label)
                    if(template){
                        const newBubble = await createGoal(template,false,activeBubble.position)
                        Composite.remove(engine.world,[activeBubble])
                        Composite.add(engine.world,[newBubble])
                        activeBubble=false
                    }
                }
            }

            // evento de long press
            Events.on(mConstraint, "mousedown", function (event) {
                if (mConstraint.body) {

                    // la siguiente linea de codigo permite remover una burbuja
                    //Composite.remove(engine.world, mConstraint.body)

                    const bubble = mConstraint.body;
                    const { position } = bubble
                    const menuVariant = getAreaClicked(position)
                    // console.log('menuVariant', menuVariant)
                    longPressTimer = setTimeout(callLongPress, 2000, bubble);
                    isShortPress=bubble
                    shortPressTimer = setTimeout(()=>isShortPress=false, 100, bubble);
                    // console.log(`${mConstraint.body.label} CLICKED`)
                }
            });

            Events.on(mConstraint, "mouseup", () => {
                if (longPressTimer) {
                    clearTimeout(longPressTimer);
                }
                if(isShortPress){
                    callShortPress(isShortPress)
                }
                
                // console.log('long press stopped')
            });

            Events.on(mConstraint, "startdrag", function (event) {
                if (mConstraint.body) {
                    mConstraint._drag = true
                }
            });
            Events.on(mConstraint, "mousemove", function (event) {
                if (shortPressTimer) {
                    // clearTimeout(shortPressTimer);
                    // isShortPress=false;
                }
                // else{
                    // create limits for the viewport
                    var extents = {
                        min: { x: 0, y: 0 },
                        max: { x: 5 * document.body.clientWidth, y: canvasSize.height }
                    };

                    // get the centre of the viewport
                    // var viewportCentre = {
                    //     x: render.options.width * 0.5,
                    //     y: render.options.height * 0.5
                    // };


                    // get vector from mouse relative to centre of viewport
                    var deltaCentre = Vector.sub(mConstraint.mouse.absolute, Vector.sub(mConstraint.mouse.mousedownPosition,mConstraint.mouse.offset)),
                    centreDist = Vector.magnitude(deltaCentre);

                    //TODO: improve attractor identification (after test 5 is always its id)
                    if (mConstraint.body && mConstraint.body.label[0]!=='_') {
                        const mouseP = Vector.add(mConstraint.mouse.absolute, mConstraint.mouse.offset)
                        const magnitude = Vector.magnitude(Vector.sub(mouseP,mConstraint.body.position))
                        Body.setPosition(mConstraint.body, mouseP)
                    }
                    // console.log('__2__', mConstraint.body)

                    if(centreDist>10 && (!mConstraint.body || mConstraint.body.label[0]==='_')){

                        // create a vector to translate the view, allowing the user to control view speed
                        var direction = Vector.neg(Vector.normalise(deltaCentre));
                        const speed = 10//Math.min(10, Math.pow(centreDist - 50, 2) * 0.0002);


                        const translate = Vector.mult(direction, speed);

                        // prevent the view moving outside the extents
                        if (render.bounds.min.x + translate.x < extents.min.x)
                            translate.x = extents.min.x - render.bounds.min.x;

                        if (render.bounds.max.x + translate.x > extents.max.x)
                            translate.x = extents.max.x - render.bounds.max.x;

                        if (render.bounds.min.y + translate.y < extents.min.y)
                            translate.y = extents.min.y - render.bounds.min.y;

                        if (render.bounds.max.y + translate.y > extents.max.y)
                            translate.y = extents.max.y - render.bounds.max.y;

                        // move the view
                        Bounds.translate(render.bounds, translate);

                        // we must update the mouse too
                        Mouse.setOffset(canvasMouse, render.bounds.min);
                }
                // }
            });

            Events.on(mConstraint, "enddrag", function (event) {
                if (mConstraint.body) {
                    mConstraint._drag = false
                }
            });

            Render.run(render);
            Runner.run(runner, engine);
        };

        init();

        return () => {
            Render.stop(render);
            Composite.clear(engine.world, true);
            Engine.clear(engine);
            Runner.stop(runner);
            render.canvas.remove();
            render.canvas = null;
            render.context = null;
            render.textures = {};
        };
    }, [engine]);

    const handleClick = () => {
        //setShowMenu(false)
    };

    return (
        <>
        <img src= './header.svg' style={{width:'100vw', height:'auto'}}/>
        <img  src= './header2.svg' style={{position:'absolute', top:70, left:0, width:'100vw', height:'auto'}}/>
        {/* <div style={{position:'absolute', padding:'0.75em 1em'}}>
            <b style={{fontSize:'40px'}}>$110</b><br/>
            <b>total money on your Vault</b>
        </div> */}
            {showMenu &&
                <BubbleOverlay
                    // width={canvasSize.width}
                    // height={canvasSize.height}
                    handleClick={handleClick}
                />}
            <div id="goals-animation"></div>
            {/* <RadialMenu /> */}
            <img src= './Button.svg' style={{position:'absolute', height:'auto', bottom:22, margin:'auto', left:0, right:0}}/>
        </>
    );
}

export default App;
