import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Routes, Route } from "react-router-dom";

import App from './App';
import Bike from './Bike'
import Iphone from './Iphone'
import EditGoal from './EditGoal'

ReactDOM.render(
  <React.StrictMode>
    <HashRouter basename={process.env.PUBLIC_URL}>
      <Routes>
        <Route path='/' element={<App />} />
        <Route path='/bike' element={<Bike />} />
        <Route path='/iphone' element={<Iphone />} />
        <Route path='/edit' element={<EditGoal />} />
      </Routes>
    </HashRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
